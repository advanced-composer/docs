---
layout: default
title: Attachment details
parent: Quickstart Guide
nav_order: 3
permalink: quickstart/attachment-details
---

# Attachment details

Use this feature to customize the default cloud files attachment info.

If default attachment info templates have been set, they will be used automatically. Otherwise, you have to choose from the popup menu which template to use.

To determine where your custom attachment info should appear in the message, place your cursor on an empty line before attaching any cloud files. In case you forgot to specify the line or the line was not valid, the attachment info would be placed at the end of your message or above the quote header.

To move your attachment info up or down press `Alt+Shift+UpArrow` or `Alt+Shift+DownArrow`.

It is not possible to change the attachment info template when you already have some files attached. To change the template you must first remove your attachments (don't delete the attachment info directly from Thunderbird editor) then choose another template.

## Examples

Unlike message templates, the examples below can be copied and pasted directly to the attachment template editor because the the editor is just a `textarea` element.

### HTML template example

Example 1:

```html
<!-- Attachment info main container -->
<div class="container" style="font-size: 10pt; font-family: Segoe UI, sans-serif;">

<span style="font-weight: bold;">Attachment List</span><br>

<!-- List container -->
<div class="list">

<!-- Item details (repeated for each attachment) -->
<div class="row" style="display: grid;
                        grid-template-columns: 0fr 1fr 0fr 0fr;
                        align-items: center;">

  <!--
    -- To change the default paper clip icon, put inline image data in the following
    -- attributes:
    -- (a) "data-src-password-protected-icon" for protected link
    -- (b) "data-src-icon" for unprotected link
    --
    -- If no attributes are supplied, the default icon will be used. Also, if no image
    -- size attributes are supplied the default sizes will be used.
    -->
  <img class="paperClipIcon" style="grid-area: 1 / 1;">
  
  <!-- All other details are self explanatory. Use the class names as clue. -->
  <span class="cloudfile-name" style="grid-area: 1 / 2;"></span>

  <span style="grid-column: 2 / span 3;">Link:
    <a class="cloudfile-link" style="grid-column: 2 / span 3;"></a>
  </span>

  <span style="grid-column: 2 / span 3;">Size:
    <span class="cloudfile-size" style="grid-column: 2 / span 3;"></span>
  </span>

  <span style="grid-column: 2 / span 3;">Expiry Date:
    <span class="cloudfile-expiry-date" style="grid-column: 2 / span 3;"></span>
  </span>

  <span style="grid-column: 2 / span 3;">Download Limit:
    <span class="cloudfile-download-limit" style="grid-column: 2 / span 3;">-</span>
  </span>

  <img class="cloudfile-service-icon" style="grid-area: 1 / 3;">

  <span class="cloudfile-service-name" style="grid-area: 1 / 4;"></span>
</div>
</div>
</div>
```

Example 2:

```html
<!-- Attachment info main container -->
<table class="container" style="width: 1000px;
                                border: 1px solid #808080;
                                border-left: none;
                                border-right: none;
                                border-collapse: collapse;
                                font-size: 10pt;
                                font-family: Segoe UI, sans-serif;">
<colgroup>
  <col style="width: 75px;">
  <col style="width: 60px;">
  <col style="width: 300px;">
  <col>
  <col span="3" style="width: 80px;">
</colgroup>

<!--
  -- List container
  --
  -- When using table as the container of the attachment details, the class "list"
  -- must be assigned to <tbody> element.
  -->
<tbody class="list">

<!-- Table header -->
<tr style="border: 1px solid #808080;
           border-left: none;
           border-right: none;">
  <td style="border: none;">File Name</td>
  <td style="border: none;">Size</td>
  <td style="border: none;">SHA-1 Sum</td>
  <td style="border: none;">Link</td>
  <td style="text-align: center; border: none;">Protected</td>
  <td style="border: none;">Expiry Date</td>
  <td style="text-align: center; border: none;">DL Limit</td>
</tr>

<!-- Item details (repeated for each cloud file) -->
<tr class="row">
  <td style="border: none;"><span class="cloudfile-name"></span></td>
  <td style="border: none;"><span class="cloudfile-size"></span></td>

  <!--
    -- Some notes about hash calculation:
    -- (a) Supported algorithms are sha-1, sha-256, sha-384, sha-512.
    -- (b) Specify the max size in bytes of attachments whose check-sums are going to be
    --     calculated in "data-max-file-size" attribute. When the size of a file is more
    --     than this value the hash will not be calculated. Setting "data-max-file-size"
    --     to a value greater than 1073741824 (1GB) will also ignore the hash calculation.
    -- (c) Hash calculation is not progressive, meaning a file will be entirely loaded to
    --     memory first then be calculated. This limitation can be problematic when the
    --     file size is large and there is not enough memory available. So it is advisable
    --     to adjust "data-max-file-size" to a reasonable value.
    -->
  <td style="border: none;">
    <span class="hash"
          data-algorithm="sha-1"
          data-max-file-size="1073741824">Calculating...</span>
  </td>

  <td style="border: none;"><a class="cloudfile-link">-</a></td>
  <td style="text-align: center; border: none;">
    <span class="cloudfile-is-link-protected"
          data-true="Yes"
          data-false="No">-</span>
  </td>
  <td style="border: none;"><span class="cloudfile-expiry-date">-</span></td>
  <td style="text-align: center; border: none;"><span class="cloudfile-download-limit">-</span></td>
</tr>
</tbody>
</table>
```

### Plain text example

```
* File name: [fileName]
  Size: [fileSize]
  Link: [fileLink]
  Hash: [hash:sha-256:1073741824]
```