---
layout: default
title: Quickstart Guide
nav_order: 2
has_children: true
---

# Quickstart Guide

{: .note }
> While Advanced Composer 3.* is now functioning well, the documentation is not complete yet. You can refer to <a href="https://gitlab.com/codergit/advanced-composer-extension#table-of-contents" target="_blank">the old documentation</a> until this documentation is ready.