---
layout: default
title: Signatures
parent: Quickstart Guide
nav_order: 2
permalink: quickstart/signatures
---

# Signature setup

If quote header feature is enabled, then signature feature must be enabled as well even when you are using Thunderbird signature.

Make sure your Thunderbird setting look like the following:

![](../assets/images/signature-setting.png)

When you have both Advanced Composer signature and Thunderbird signature for the same account, then Advanced Composer signature will be used. If you want to use Thunderbird signature instead, empty the Advanced Composer signature.