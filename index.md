---
layout: default
title: Home
nav_order: 1
description: "Advanced Composer is a multipurpose message composition extension for Mozilla Thunderbird."
permalink: /
---

# Intelligently compose your emails with ease
{: .fs-8 }

Advanced Composer is a message composition extension for Mozilla Thunderbird with many features, ranging from inserting templates stored locally on your computer to attaching files from a remote location.
{: .fs-6 .fw-300 }

![](assets/images/options-page-general.png)

{: .note }
> While Advanced Composer 3.* is now functioning well, the documentation is not complete yet. You can refer to <a href="https://gitlab.com/codergit/advanced-composer-extension#table-of-contents" target="_blank">the old documentation</a> until this documentation is ready.
>
> If you find something unclear or wrong in the documentation you can ask a question on [Gitlab issue page](https://gitlab.com/codergit/advanced-composer-extension/-/issues). But before asking your question, make some efforts to ensure that it is truly an issue. Please bear in mind that most of the addon developers are volunteers; they have their own full time jobs and only work on the addon development in their spare time.

---

## Features

Brief descriptions of the features of Advanced Composer are as per table below.

| Features                                                     | Notes | 
|:-------------------------------------------------------------|:------|
| Config backup and restore.                                   | Backups are tightly related to email accounts. This means a backup for one Thunderbird installation can only be imported to another installation if both have the same account configurations. |
| Message templates in both plaintext and HTML formats.        | -     |
| Custom attachment details with checksum support.             | Only for Filelink attachments. |
| Fixed variables.                                             | Variables whose values and names are fixed; user cannot changed them. |
| Static user-defined variables.                               | Variables whose values and names are user-definable. |
| Dynamic user-defined variables.                              | Similar to static user-defined variables but the value is fetched from a server. |
| Message signature for each mail account.                     | -     |
| Predefined message recipient groups.                         | An easy way to fill in the address fields with recipients when composing a message. |
| Quote header in Outlook style.                               | -     |

{: .note }
In the next few months the *dynamic user-defined variable* feature starting from version 3.2 will be limited to about twenty queries per month. To bypass this restriction a key is required which costs a few bucks a year. Please consider this decision as an appeal to support the continued maintenance of this extension.

## Prerequisites
- Make sure your Thunderbird version is at least 102.0.
- For Linux based systems, make sure you have *msttcorefonts* installed for standard fonts. Otherwise, you will find no fonts listed on the option page of this extension.

## Installation
- From the Thunderbird add-ons manager search `Advanced Composer` then click the button `Add to Thunderbird`, or
- Download the extension package from <a href="https://addons.thunderbird.net/en-US/thunderbird/addon/advanced-composer/" target="_blank">https://addons.thunderbird.net/en-US/thunderbird/addon/advanced-composer/</a> then from the add-ons manager click the gear icon and select `Install Add-on From File...`.
