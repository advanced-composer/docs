---
layout: default
title: Variables
parent: Quickstart Guide
nav_order: 5
permalink: quickstart/variables
---

# Variables
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## General notes

Variables can be used in message templates, signatures, attachment details. Each variable must be enclosed with `${}`.

A variable with format `HTML` must be placed on its own line. There must be no other texts before and after the variable.

A variable can contain another variable but a variable cannot contain a template.

A plain text variable value can be tranformed into HTML using this format `${variableName[a][b][c]}`, where `a` is an HTML tag, `b` HTML attributes and `c` separator for variables with value type of array.

Valid variable name can only contain alphanumeric plus dot and underscore. Variable name is case insensitive.

## Fixed variables

Fixed variables associated with the message being composed are prefixed with `cm` and the original message `om`.

## User-defined variables

User-defined variables both static and dynamic allow users to create their own variables and give them more control over the variable values.

Use the variable manager to manage your user-defined variables:
- Use buttons `Add` and `Remove` to add/delete a variable. Deletion can not be undone; always make a backup of your config before making any changes.
- Use `Enter` key to confirm variable creation.
- To edit a variable name, double click the variable or press `F2`. Press `Enter` to confirm changes.

To use dynamic variables, a server is required. It does not mean the server must be somewhere in a far away land or in a data center because you can even use your localhost as a server. It is also essential to understand what CORS is and how to deal with it for a successful client-server communication.

Important notes about dynamic user-defined variables:
- Supported HTTP request methods are `POST` and `GET`.
- For `POST` request, the request body is always sent as a JSON string.
- The text area `arguments` of the variable manager is where to set the request data. The data must be in key-value pairs where both value and key must be double-quoted. The value can also be set to any fixed variable, e.g. `"originalEmailFile": "${om.eml}", "composedMsgType": "${cm.type}"`.
- Variable name, format and type are always included in the request data.
- For both `POST` and `GET` requests, the server must return HTTP status code `200` for each successful request and the response body must be a JSON string.

{: .warning }
Extra care must be taken into consideration when using dynamic user-defined variables. Imagine e.g., passing `${om.eml}` to an evil server.

## Example

1. Create a static variable with the following properties:
    - name: `hotDogsIngredients`
    - format: `plain text`
    - type: `array`
    - value: `["ingredient one", "ingredient two", "ingredient three", "ingredient four", "ingredient five"]`
2. Create another static variable with the following properties:
    - name: `ingredientsOrderedList`
    - format: `HTML`
    - type: `scalar`
    - value: `<ol>${hotDogsIngredients[li][][]}</ol>`
3. Create a dynamic variable with the following properties:
    - name: `greeting`
    - format: `plain text`
    - type: `scalar`
    - resource: `http://localhost:8080`
4. Create another dynamic variable with the following properties:
    - name: `attachment`
    - format: `file`
    - type: `scalar`
    - arguments: `"path": "D:/attachments/recipe.pdf"`
    - resource: `http://localhost:8080`
5. Create an HTML template and set its content to the example below. Do not copy-paste it to the template editor but write it manually instead or alternatively put it into an intermediary variable and set that variable as the only content of the template.
6. Run [example server](server).

```
${greeting} ${cm.to.composeName[span][style="color: dodgerblue"][, ]}.

At your request here is the list of ingredients commonly used to make hot dogs:
${ingredientsOrderedList}

I also attached a PDF file containing not only ingredients but also step-by-step
guides for you to follow along.
${attachment}
```

{: .note }
It is recommended to always put dynamic variables with format `file` at the end of the template. Upon template expansion completion they (the variables not the files) will be removed from the template.