---
layout: default
title: Message templates
parent: Quickstart Guide
nav_order: 1
permalink: quickstart/message-templates
---

# Message templates

The template editor features are currently very basic. However, as the variable support is now available there is an easier way to set the content of a template by using an intermediary variable. For example:
- Create a static variable e.g., `myIntermediaryVar` and set the value type as `scalar`.
- Set the value of the variable to whatever content you want your template to have.
- Set the content of your template to `${myIntermediaryVar}`.

## Creating a message template

To create a message template, go to tab `Message Templates` of the options page.

As of version 3.0, a new insertion method `cursor` is used by default when creating a new template.

## Inserting a message template

In addition to context menu, shortcut and template placeholder, a popup menu is a new method of inserting a template to Thunderbird editor.

To show the popup menu, find a triangle at the upper left corner of the editor and move your cursor over there until an animation is shown. Roll the middle button of your mouse.

To filter the templates, type something in the search input of the popup menu. Case insensitive regular expression is also supported, e.g. `~^sale\d+$`. To paste some text into the search input, use `Ctrl+Shift+V`.

To insert a template at cursor location:
- Make sure the insert mode of the template you are going to insert is set to `cursor`.
- In the Thunderbird editor, place the cursor on a empty line then insert your message.
