---
layout: default
title: Quote header
parent: Quickstart Guide
nav_order: 4
permalink: quickstart/quote
---

# Quote header

If for whatever reason you think the current quote header does not suit your need, you can create your own quote header.

Here is an RTL quote header example:
1. Open the options page.
2. Create a new HTML template and set its content to `${rtlQuoteHeader}`.
3. Go to tab `General` and set the newly created template as the default template for your account of choice.
4. Create a new dynamic user-defined variable with the following properties:
  - name : `rtlQuoteHeader`
  - format: `html`
  - type: `scalar`
  - arguments: `"senderName": "${om.from.name}", "senderEmail": "${om.from.email}", "toName": "${om.to.name}", "toEmail": "${om.to.email}", "ccName": "${om.cc.name}", "ccEmail": "${om.cc.email}", "subject": "${om.subject}", "date": "${om.date}", "messageType": "${cm.type}"`
5. Go to tab `Quote header` and disable the quote header for your account of choice.
6. Go to tab `General` and disable signature feature.
7. Run [server example](server).

{: .warning }
I have no knowledge in reading or writing or even coding HTML in any RTL languages. This example is for demonstration purpose only and the rendered HTML result may be wrong.